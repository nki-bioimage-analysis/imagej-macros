// "StartupMacros"
// The macros and macro tools in this file ("StartupMacros.txt") are
// automatically installed in the Plugins>Macros submenu and
//  in the tool bar when ImageJ starts up.

//  About the drawing tools.
//
//  This is a set of drawing tools similar to the pencil, paintbrush,
//  eraser and flood fill (paint bucket) tools in NIH Image. The
//  pencil and paintbrush draw in the current foreground color
//  and the eraser draws in the current background color. The
//  flood fill tool fills the selected area using the foreground color.
//  Hold down the alt key to have the pencil and paintbrush draw
//  using the background color or to have the flood fill tool fill
//  using the background color. Set the foreground and background
//  colors by double-clicking on the flood fill tool or on the eye
//  dropper tool.  Double-click on the pencil, paintbrush or eraser
//  tool  to set the drawing width for that tool.
//
// Icons contributed by Tony Collins.

// Global variables
var pencilWidth=1,  eraserWidth=10, leftClick=16, alt=8;
var brushWidth = 10; //call("ij.Prefs.get", "startup.brush", "10");
var floodType =  "8-connected"; //call("ij.Prefs.get", "startup.flood", "8-connected");

// The macro named "AutoRunAndHide" runs when ImageJ starts
// and the file containing it is not displayed when ImageJ opens it.

// macro "AutoRunAndHide" {}

function UseHEFT {
	requires("1.38f");
	state = call("ij.io.Opener.getOpenUsingPlugins");
	if (state=="false") {
		setOption("OpenUsingPlugins", true);
		showStatus("TRUE (images opened by HandleExtraFileTypes)");
	} else {
		setOption("OpenUsingPlugins", false);
		showStatus("FALSE (images opened by ImageJ)");
	}
}

UseHEFT();

// The macro named "AutoRun" runs when ImageJ starts.

macro "AutoRun" {
	// run all the .ijm scripts provided in macros/AutoRun/
	autoRunDirectory = getDirectory("imagej") + "/macros/AutoRun/";
	if (File.isDirectory(autoRunDirectory)) {
		list = getFileList(autoRunDirectory);
		// make sure startup order is consistent
		Array.sort(list);
		for (i = 0; i < list.length; i++) {
			if (endsWith(list[i], ".ijm")) {
				runMacro(autoRunDirectory + list[i]);
			}
		}
	}
}

var pmCmds = newMenu("Popup Menu",
	newArray("Help...", "Rename...", "Duplicate...", "Original Scale",
	"Paste Control...", "-", "Record...", "Capture Screen ", "Monitor Memory...",
	"Find Commands...", "Control Panel...", "Startup Macros...", "Search..."));

macro "Popup Menu" {
	cmd = getArgument();
	if (cmd=="Help...")
		showMessage("About Popup Menu",
			"To customize this menu, edit the line that starts with\n\"var pmCmds\" in ImageJ/macros/StartupMacros.txt.");
	else
		run(cmd);
}

macro "Abort Macro or Plugin (or press Esc key) Action Tool - CbooP51b1f5fbbf5f1b15510T5c10X" {
	setKeyDown("Esc");
}

var xx = requires138b(); // check version at install
function requires138b() {requires("1.38b"); return 0; }

var dCmds = newMenu("Developer Menu Tool",
newArray("ImageJ Website","News", "Documentation", "ImageJ Wiki", "Resources", "Macro Language", "Macros",
	"Macro Functions", "Startup Macros...", "Plugins", "Source Code", "Mailing List Archives", "-", "Record...",
	"Capture Screen ", "Monitor Memory...", "List Commands...", "Control Panel...", "Search...", "Debug Mode"));

macro "Developer Menu Tool - C037T0b11DT7b09eTcb09v" {
	cmd = getArgument();
	if (cmd=="ImageJ Website")
		run("URL...", "url=http://rsbweb.nih.gov/ij/");
	else if (cmd=="News")
		run("URL...", "url=http://rsbweb.nih.gov/ij/notes.html");
	else if (cmd=="Documentation")
		run("URL...", "url=http://rsbweb.nih.gov/ij/docs/");
	else if (cmd=="ImageJ Wiki")
		run("URL...", "url=http://imagejdocu.tudor.lu/imagej-documentation-wiki/");
	else if (cmd=="Resources")
		run("URL...", "url=http://rsbweb.nih.gov/ij/developer/");
	else if (cmd=="Macro Language")
		run("URL...", "url=http://rsbweb.nih.gov/ij/developer/macro/macros.html");
	else if (cmd=="Macros")
		run("URL...", "url=http://rsbweb.nih.gov/ij/macros/");
	else if (cmd=="Macro Functions")
		run("URL...", "url=http://rsbweb.nih.gov/ij/developer/macro/functions.html");
	else if (cmd=="Plugins")
		run("URL...", "url=http://rsbweb.nih.gov/ij/plugins/");
	else if (cmd=="Source Code")
		run("URL...", "url=http://rsbweb.nih.gov/ij/developer/source/");
	else if (cmd=="Mailing List Archives")
		run("URL...", "url=https://list.nih.gov/archives/imagej.html");
	else if (cmd=="Debug Mode")
		setOption("DebugMode", true);
	else if (cmd!="-")
		run(cmd);
}

var sCmds = newMenu("Stacks Menu Tool",
	newArray("Add Slice", "Delete Slice", "Next Slice [>]", "Previous Slice [<]", "Set Slice...", "-",
		"Convert Images to Stack", "Convert Stack to Images", "Make Montage...", "Reslice [/]...", "Z Project...",
		"3D Project...", "Plot Z-axis Profile", "-", "Start Animation", "Stop Animation", "Animation Options...",
		"-", "MRI Stack (528K)"));
macro "Stacks Menu Tool - C037T0b11ST8b09tTcb09k" {
	cmd = getArgument();
	if (cmd!="-") run(cmd);
}

var luts = getLutMenu();
var lCmds = newMenu("LUT Menu Tool", luts);
macro "LUT Menu Tool - C037T0b11LT6b09UTcb09T" {
	cmd = getArgument();
	if (cmd!="-") run(cmd);
}
function getLutMenu() {
	list = getLutList();
	menu = newArray(16+list.length);
	menu[0] = "Invert LUT"; menu[1] = "Apply LUT"; menu[2] = "-";
	menu[3] = "Fire"; menu[4] = "Grays"; menu[5] = "Ice";
	menu[6] = "Spectrum"; menu[7] = "3-3-2 RGB"; menu[8] = "Red";
	menu[9] = "Green"; menu[10] = "Blue"; menu[11] = "Cyan";
	menu[12] = "Magenta"; menu[13] = "Yellow"; menu[14] = "Red/Green";
	menu[15] = "-";
	for (i=0; i<list.length; i++)
		menu[i+16] = list[i];
	return menu;
}

function getLutList() {
	lutdir = getDirectory("luts");
	list = newArray("No LUTs in /ImageJ/luts");
	if (!File.exists(lutdir))
		return list;
	rawlist = getFileList(lutdir);
	if (rawlist.length==0)
		return list;
	count = 0;
	for (i=0; i< rawlist.length; i++)
		if (endsWith(rawlist[i], ".lut")) count++;
	if (count==0)
		return list;
	list = newArray(count);
	index = 0;
	for (i=0; i< rawlist.length; i++) {
		if (endsWith(rawlist[i], ".lut"))
			list[index++] = substring(rawlist[i], 0, lengthOf(rawlist[i])-4);
	}
	return list;
}

macro "Pencil Tool - C037L494fL4990L90b0Lc1c3L82a4Lb58bL7c4fDb4L5a5dL6b6cD7b" {
	getCursorLoc(x, y, z, flags);
	if (flags&alt!=0)
		setColorToBackgound();
	draw(pencilWidth);
}

macro "Paintbrush Tool - C037La077Ld098L6859L4a2fL2f4fL3f99L5e9bL9b98L6888L5e8dL888c" {
	getCursorLoc(x, y, z, flags);
	if (flags&alt!=0)
		setColorToBackgound();
	draw(brushWidth);
}

macro "Flood Fill Tool -C037B21P085373b75d0L4d1aL3135L4050L6166D57D77D68La5adLb6bcD09D94" {
	requires("1.34j");
	setupUndo();
	getCursorLoc(x, y, z, flags);
	if (flags&alt!=0) setColorToBackgound();
	floodFill(x, y, floodType);
}

function draw(width) {
	requires("1.32g");
	setupUndo();
	getCursorLoc(x, y, z, flags);
	setLineWidth(width);
	moveTo(x,y);
	x2=-1; y2=-1;
	while (true) {
		getCursorLoc(x, y, z, flags);
		if (flags&leftClick==0) exit();
		if (x!=x2 || y!=y2)
			lineTo(x,y);
		x2=x; y2 =y;
		wait(10);
	}
}

function setColorToBackgound() {
	savep = getPixel(0, 0);
	makeRectangle(0, 0, 1, 1);
	run("Clear");
	background = getPixel(0, 0);
	run("Select None");
	setPixel(0, 0, savep);
	setColor(background);
}

// Runs when the user double-clicks on the pencil tool icon
macro 'Pencil Tool Options...' {
	pencilWidth = getNumber("Pencil Width (pixels):", pencilWidth);
}

// Runs when the user double-clicks on the paint brush tool icon
macro 'Paintbrush Tool Options...' {
	brushWidth = getNumber("Brush Width (pixels):", brushWidth);
	call("ij.Prefs.set", "startup.brush", brushWidth);
}

// Runs when the user double-clicks on the flood fill tool icon
macro 'Flood Fill Tool Options...' {
	Dialog.create("Flood Fill Tool");
	Dialog.addChoice("Flood Type:", newArray("4-connected", "8-connected"), floodType);
	Dialog.show();
	floodType = Dialog.getChoice();
	call("ij.Prefs.set", "startup.flood", floodType);
}

macro "Set Drawing Color..."{
	run("Color Picker...");
}

macro "-" {} //menu divider

macro "About Startup Macros..." {
	title = "About Startup Macros";
	text = "Macros, such as this one, contained in a file named\n"
		+ "'StartupMacros.txt', located in the 'macros' folder inside the\n"
		+ "Fiji folder, are automatically installed in the Plugins>Macros\n"
		+ "menu when Fiji starts.\n"
		+ "\n"
		+ "More information is available at:\n"
		+ "<http://imagej.nih.gov/ij/developer/macro/macros.html>";
	dummy = call("fiji.FijiTools.openEditor", title, text);
}

macro "Save As JPEG... [j]" {
	quality = call("ij.plugin.JpegWriter.getQuality");
	quality = getNumber("JPEG quality (0-100):", quality);
	run("Input/Output...", "jpeg="+quality);
	saveAs("Jpeg");
}

macro "Save Inverted FITS" {
	run("Flip Vertically");
	run("FITS...", "");
	run("Flip Vertically");
}

var onmouseover_magnify_radius=40;
var onmouseover_magnify_scale=3;
var onmouseover_RGB_all_channels=true;

macro "Onmouseover Magnify Tool - C000D0eD0fD1dD1eD2cD2dD3bD3cD4aD4bD5aC000C111C222D59C222C333C444C555C666C777D68D78C777Dd1C777D71C777Dd8C777D62C777De7C777D67De2C888Db9C888D99C888D89C888Da9C888D90Db0Dc9De3C888De6C888D80C888C999D63Da0C999Dc0C999D66C999D55C999D54D69C999De4De5C999D1fC999D64D65Dc8CaaaDd2CaaaD2eCaaaD88Dd7CaaaD3dDa2Df5CaaaD92Db2Df4CaaaD4cD5bD72D74D75D81D83D84D85D86D87D93D94D95D96D97Da3Da4Da5Da6Da7Db3Db4Db5Db6Db7Dc3Dc4Dc5Dc6CaaaD82Dc7Dd4Dd5CaaaD77D98Da8Db8CaaaD73D76Dd3Dd6CaaaDc2CaaaD49Dc1CaaaCbbbD3aCbbbD2bCbbbD1cCbbbD0dCbbbD79CbbbCcccDa1CcccD91CcccDb1CcccCdddCeeeCfff"{

	//Runs when clicking left mouse button
	leftButton=16;
	setBatchMode(true);
	image = getTitle();
	Overlay.remove;
	getDimensions(width, height, channels, slices, frames);
	if(onmouseover_magnify_radius*onmouseover_magnify_scale > minOf(width, height)/2) exit("The magnified window is larger than the image.\nPlease select a smaller Magnify radius and/or Magnify factor.\n(Double-click the icon in the Fiji toolbar)");
	makeRGB = false;
	if(channels > 1 && onmouseover_RGB_all_channels == true) makeRGB = true;
	x1 = -1;
	y1 = -1;
	getCursorLoc(x, y, z, flags);
	while(flags&leftButton != 0){
		if(x>0 && x<width && y>0 && y<height) {
			if(x != x1 || y != y1) {
				x1 = x;
				y1 = y;
				selectWindow(image);
				makeRectangle(maxOf(minOf(x-onmouseover_magnify_radius,width),0), maxOf(minOf(y-onmouseover_magnify_radius,height),0), onmouseover_magnify_radius*2, onmouseover_magnify_radius*2);
				if(makeRGB) {
					if(slices>1 || frames>1) {
						Stack.getPosition(current_channel, current_slice, current_frame);
						run("Duplicate...", "title=ROI_RGB duplicate slices="+current_slice+" frames="+current_frame);
					}
					else run("Duplicate...", "title=ROI_RGB duplicate");
					run("RGB Color");
				}
				run("Scale...", "x=" + onmouseover_magnify_scale + " y=" + onmouseover_magnify_scale + " interpolation=None average create title=onmouseover_magnify_scaled_ROI");
				selectWindow(image);
				run("Select None");
				run("Add Image...", "image=onmouseover_magnify_scaled_ROI x=" + x - onmouseover_magnify_scale*onmouseover_magnify_radius + " y=" + y - onmouseover_magnify_scale*onmouseover_magnify_radius + " opacity=100");
				makeRectangle(x - onmouseover_magnify_scale*onmouseover_magnify_radius, y - onmouseover_magnify_scale*onmouseover_magnify_radius, onmouseover_magnify_scale*onmouseover_magnify_radius*2, onmouseover_magnify_scale*onmouseover_magnify_radius*2);
				run("Add Selection...");
				if (Overlay.size==4) {
					Overlay.removeSelection(1);
					Overlay.removeSelection(0);
				}
				close("onmouseover_magnify_scaled_ROI");
				if(makeRGB) close("ROI_RGB");
			}
		}
		wait(25);
		getCursorLoc(x, y, z, flags);
	}
	run("Select None");
	Overlay.remove;
}

macro "Onmouseover Magnify Tool Options" {
	Dialog.create("Magnify Options");
	Dialog.addNumber("Magnify radius", onmouseover_magnify_radius);
	Dialog.addNumber("Magnify factor", onmouseover_magnify_scale);
	Dialog.addCheckbox("Magnify all channels?", onmouseover_RGB_all_channels);
	Dialog.show();
	onmouseover_magnify_radius = Dialog.getNumber();
    onmouseover_magnify_scale = Dialog.getNumber();
    onmouseover_RGB_all_channels = Dialog.getCheckbox();
}


macro "Onmouseover Magnify Tool Selected" {
	//Do something here
}

var operationMode = "Single trace onMouseOver";
var smoothTraces = 1;
var selectedTraceLineWidth = 4.0;
var cellSelectionSize = 10;
var useTraceColor = false;


macro "Toggle Overlay [f9]" {
	if (Overlay.size>0) {
		if (Overlay.hidden) Overlay.show;
		else Overlay.hide;
	}
}


macro "Select Traces Tool - C314DbeDcdC314DbfC315DddC315DbdC325DadC326DdeC326C327D02D11C327C328DccC338D13C339D03C339C44aC44bC45bD21C45cD22C45cC45dC46dD14C46dC46eDaeDbcC47eC47fC48fD04D0cC48fDedC48fDfbDfeC48fD0eD8eC49fD9fC49fD1eC49fDe5DebC49fDdcC39fD0dD1cDffC39fD2fC3afD15D2bDcbC3afD0fD8fDf4C3afD7eDdaC3afD7fC3afD1bC3afD1fD2eD3cD3fDe2Df5DfcC2bfDf3C2bfD00D2cD34C2bfD1dD5fDd9C2beD0bD6eDefC2beD20DafDdfDeaC2beD6fDf2C2ceD23D4fC2ceD76De3Df1DfaC2ceC2cdD6dD9eDe6C1cdD2dDecC1cdDe9Df9C1cdD3bDf0C1ddD5eD7dC1ddD01De4C1ddD3eC1dcD3dDe8DeeC1dcD0aD5dD9aC1dcD06D1aD30D40D58Df6C1dcD44D4dDe7C1dcD57Dc3De0C1ebD16D25D9dDd3DdbDf8C1ebD4eD67C1ebD24Dd2C1ebD10C1ebD33D68C1eaC2eaDd1Df7C2eaD4cD66D69D70C2eaD05D31D8dC2eaD2aC2eaD17Da9C2f9D59Db6C2f9De1C3f9D19DfdC3f9D60C3f9D92C3f8D41D82C4f8D77C4f8D51C4f8D6aDc2Dd0C4f7Dd8C5f7D32C5f7D8aC5f7C5f6C6f6D50C6f6D47C6f6D43C6f6Da6C7f5D7aC7f5D35D78C7f5D48Db7C7f5D8bDc1C8f5D29D71D86Da8C8f5D07D79C8f4D61Db3Dc0C8f4D80C8f4D18D95Dd7C9f4D09C9f4Dd4C9f3D53D54D5aCaf3D7bDa2Db8Caf3D81D99Caf3D63D72Db2Caf3D42Da7Caf3Cbf3Da5Cbf3D62Cbf3D73Cbf3D08Cbf3D52D96Ccf3Cce3D85Cce3D5cCce3Cde3Db1Cde3D87DacCde3D83Cde3Cdd3D89Ced3D97Db0Ced3D98DaaCed3D26D88Ced3D91DcaDd5Ced3Da3Cec3Da1Cec3Cfc3D90Cfc3Da0Dd6Cfc3D49Cfb3D28Cfb3D56Cfb3Dc4Cfb3D6bCfb3D93Cfb3Cfa3D6cCfa3D27Cfa3D9bCfa3Cf92DbbCf92D45Cf92D64Cf92D7cCf82D4bDb5Cf82D38D75Cf82D3aCf82Cf72D37Cf71Cf61Db9Cf61D5bCf61D8cCf61Cf51Ce51Ce50Ce40Cd30Cd20Dc9Cc20D4aD9cCc20Cb20Cb10Db4Cb10D46Cb10Ca10Dc7Ca10Dc6Ca10D65Ca10C910C900D74C900D94C900C800D39Dc8C700D84C700D36D55Da4DabDbaDc5"{

//Initialize
run("CLIJ2 Macro Extensions", "cl_device=");
Ext.CLIJ2_clear();
run("Clear Results");
close("Results");
roiManager("deselect");
roiManager("Set Color", "gray");
roiManager("Set Line Width", 1);
use_coordinates = false;

//Close previous plots, if present
window = select_image_containing_string("selected cells");
if(window != 0) close();
window = select_image_containing_string("highlighted");
if(window != 0) close();
window = select_image_containing_string("kymograph_smoothed");
if(window != 0) close();
window = select_image_containing_string("kymograph_sorted_smoothed");
if(window != 0) close();

plot = select_image_containing_string("traces plot");
setLocation(100, 50);
getLocationAndSize(x, y, width, height);
Overlay.remove;
plotname = getTitle();
basename = substring(plotname, 0, indexOf(plotname, " (lifetime traces plot)"));
plot_id = getImageID();
selectImage(plot_id);
print("\\Clear");
run("NKI get plot styles to log window");		//Run the groovy script to get the plot styles
logWindow = getInfo("log");
styles = split(logWindow, "\n");
print("\\Clear");

nr_cells = styles.length;

selectedTraces = newArray(0);
print("Retrieving values from "+nr_cells+" traces...");

//By doing this only once, time is saved when running multiple times.
//BUT: if the smooth radius is changed by the user Plot Values has to be closed manually in order to have effect on the selected traces plot!
PlotValuesTable = "Plot Values";
if(!isOpen(PlotValuesTable)) {
	Plot.showValues();	//Put all traces into the Results table
	selectWindow("Results");
	IJ.renameResults(PlotValuesTable);
}

kymograph_sorted = select_image_containing_string("kymograph sorted");
Overlay.remove;
setLocation(x + width + 2, 50);
getLocationAndSize(x, y, width, height);
kymograph_sorted_smoothed = select_image_containing_string("kymograph_sorted_smoothed");
Overlay.remove;
getDimensions(width, height, channels, slices, frames);
kymograph = select_image_containing_string("(kymograph)");
setLocation(screenWidth-200, screenHeight-200);
rank_vector = select_image_containing_string("rank vector");
rank_array = image1DToArray(rank_vector);
setLocation(screenWidth-200, screenHeight-200);
labelmap = select_image_containing_string("labelmap_cells");
getLut(reds, greens, blues);
setLocation(screenWidth-200, screenHeight-200);
Overlay.remove;
run("Select None");
RGB_overlay = select_image_containing_string("RGB");
setLocation(x, y + height + 70);
Overlay.remove;
run("Select None");
roiManager("Show All without labels");

frameInterval = Stack.getFrameInterval();
selectWindow(kymograph);
getDimensions(width, height, channels, slices, frames);
timeArray = Array.getSequence(height);
timeArray = multiplyArraywithScalar(timeArray, frameInterval);
timepoints = Table.size(PlotValuesTable);

//create smooth kymographs
if(smoothTraces > 0) {
	kymograph_smoothed = "kymograph_smoothed";
	Ext.CLIJ2_push(kymograph);
	Ext.CLIJ2_mean2DBox(kymograph, kymograph_smoothed, 0, smoothTraces);
	Ext.CLIJ2_pull(kymograph_smoothed);
	Ext.CLIJ2_release(kymograph_smoothed);
	selectWindow(kymograph_smoothed);
	setLocation(screenWidth-200, screenHeight-200);

	kymograph_sorted_smoothed = "kymograph_sorted_smoothed";
	selectWindow(kymograph_sorted);
	getLut(reds_kymo, greens_kymo, blues_kymo);
	Ext.CLIJ2_push(kymograph_sorted);
	Ext.CLIJ2_mean2DBox(kymograph_sorted, kymograph_sorted_smoothed, 0, smoothTraces);
	Ext.CLIJ2_pull(kymograph_sorted_smoothed);
	Ext.CLIJ2_release(kymograph_sorted_smoothed);
	selectWindow(kymograph_sorted_smoothed);
	setLut(reds_kymo, greens_kymo, blues_kymo);
	setLocation(x + width + 20, y);
	
}
else kymograph_smoothed = kymograph;


call("ij.gui.ImageWindow.setNextLocation", 100, 50);
plot = plot_traces_from_kymograph(plot, kymograph_smoothed, newArray(0));	//Recreate the plot from the kymograph (faster than resetting the styles for many traces)
selectImage(plot_id);
getLocationAndSize(x, y, width, height);
run("Close");

selectWindow(plot);
selectTracesToolID = toolID;

if(operationMode == "Select multiple traces") {
	setTool("freehand");
	
	run("Colors...", "foreground=white background=black selection=red");
	run("Roi Defaults...", "color=red stroke=5 group=0");
	waitForUser("Create \n* a ROI in the traces plot\n* a ROI or multi-point selection in the RGB overlay\n* a ROI in the sorted kymograph image\n to highlight cells");
	run("Roi Defaults...", "color=red stroke=1 group=0");
	getLocationAndSize(x_coord, y_coord, width, height);
	if(selectionType == -1) exit("No Selection found");
	if(selectionType == 10) run("Enlarge...", "enlarge="+cellSelectionSize);	//Enlarge (multi)point selections
	selectedTraces = newArray(nr_cells);
	
	setTool(selectTracesToolID);
	setBatchMode(true);
	
	
	//Check where the ROI is and get the selected cells
	if(getTitle() == plot) {
		selection = "plot";
		run("Add Selection...");
		run("Properties... ", "  fill=#220096ff");
		run("Add Selection...");
		run("Properties... ", "  stroke=black");
		n = 0;									//counter for selected traces
		for (i = 0; i <nr_cells; i++) {			//Loop over all traces
			yValues = Table.getColumn("Y"+i, PlotValuesTable);
			for (t = 0; t < timepoints; t++) {	//Check for all timepoints if there are data points inside the selection
				x = timeArray[t];
				y = yValues[t];
				toUnscaled(x, y);
				if (selectionContains(x, y)) {
					selectedTraces[n] = i;
					n++;
					break;
				}
			}
		}
		selectedTraces = Array.trim(selectedTraces, n);
		print(selectedTraces.length+" traces in selection:");
		Array.print(selectedTraces);
	}
	else if(getTitle() == RGB_overlay) {
		selection = "RGB";
		run("Properties... ", "  stroke=cyan width=3");
		run("Add Selection...");
		Overlay.setPosition(0);
		roiManager("add");						//Temporarily add the selection to the ROI manager
		run("Clear Results");
		selectWindow(labelmap);
		roiManager("select", nr_cells);			//Remove the selection again
		roiManager("delete");
		Ext.CLIJ2_push(labelmap);
		Ext.CLIJ2_statisticsOfLabelledPixels(labelmap, labelmap);
		Ext.CLIJ2_release(labelmap);
		X = Table.getColumn("CENTROID_X", "Results");
		Y = Table.getColumn("CENTROID_Y", "Results");
		n = 0;									//counter for selected traces
			for (i = 0; i <nr_cells; i++) {
				if (selectionContains(X[i], Y[i])) {
					selectedTraces[n] = i;
					n++;
			}
		}
		selectedTraces = Array.trim(selectedTraces, n);
		print(selectedTraces.length+" traces in selection:");
		Array.print(selectedTraces);
	}
	else if(getTitle() == kymograph_sorted || getTitle() == kymograph_sorted_smoothed) {
		selection = "kymograph";
		if(selectionType()>3) exit("In the kymograph an area ROI is required");
		getSelectionBounds(x0, y, width, height);
		x1 = x0 + width;
		selectedTraces = Array.slice(rank_array, x0, x1);
		selectWindow(kymograph_sorted);
		getDimensions(width, height, channels, slices, frames);
		makeRectangle(x0, 0, x1-x0, height);
		Overlay.addSelection("", 0, "#55000000");
		run("Select None");
		if(smoothTraces>0) {
			selectWindow(kymograph_sorted_smoothed);
			getDimensions(width, height, channels, slices, frames);
			makeRectangle(x0, 0, x1-x0, height);
			Overlay.addSelection("", 0, "#55000000");
			run("Select None");
		}
	}
	else exit("No selection found. Please select a region in the Plot, in the RGB overlay, or in the sorted kymograph.");

	run("Colors...", "foreground=white background=black selection=cyan");
	
	plot_highlighted = plot_traces_from_kymograph(plot, kymograph_smoothed, selectedTraces);
	rename(plot + " highlighted");
	getLocationAndSize(x, y, width, height);
	
	//Create Plot of selected traces
	call("ij.gui.ImageWindow.setNextLocation", 100, y + height + 10);
	plot_selectedOnly = plot_traces(plot, PlotValuesTable, nr_cells, true, selectedTraces);
	rename(plot + " selected cells");
	
	highlight_cells_in_RGB_overlay();
	create_selected_cells_table(selection);
	close(kymograph_smoothed);
}


else if (operationMode == "Single trace onMouseOver") {
	//Runs when clicking left mouse button
	setOption("DisablePopupMenu", true);
	leftButton=16;
	x1 = -1;
	y1 = -1;
	getDimensions(width, height, channels, slices, frames);
	getCursorLoc(x, y, z, flags);
	currentCell = -1;
	oldCell = -1;
	plot_single_cell = "";
	hidden=false;

	selectWindow(kymograph_sorted);
	setTool(selectTracesToolID);
	while(toolID == selectTracesToolID) {
		if(getTitle() == RGB_overlay) {
			setBatchMode(true);
			Stack.getPosition(channel, slice, frame);
			selectWindow(labelmap);
			currentCell = getPixel(x, y) - 1;	//labels start at 1
			selectWindow(RGB_overlay);
			if(currentCell != -1) showStatus("cell "+currentCell + 1);
			else showStatus("background");
			style = getStyle(0, styles);
			if(currentCell != -1) {
				style = getStyle(currentCell, styles);
				roiManager("select", currentCell);
				if(useTraceColor == true) roiManager("Set Color", style[0]);
				else roiManager("Set Color", "white");
				roiManager("Set Line Width", 3);
				run("Select None");
				if(currentCell != oldCell && oldCell != -1) {	//Reset all ROIs to gray
					roiManager("Select", oldCell);
					roiManager("Set Color", "gray");
					roiManager("Set Line Width", 1);
					run("Select None");
				}
				
				show_line_on_sorted_kymograph(kymograph_sorted_smoothed, rank_array, currentCell, 2);
				show_line_on_sorted_kymograph(kymograph_sorted, rank_array, currentCell, 2);
//				show_horizontal_line_on_sorted_kymograph(kymograph_sorted, frame, 1);
//				show_horizontal_line_on_sorted_kymograph(kymograph_sorted_smoothed, frame, 1);
				
				selectWindow(RGB_overlay);
				if(flags&leftButton != 0) {	//leftclick to show a single trace 
					selectWindow(plot);
					Plot.freeze(true);
					for (i = 0; i < nr_cells; i++) {
						//Plot.setStyle(i, styles[i]+",hidden");
						style = getStyle(i, styles);
						if(i != currentCell) Plot.setStyle(i, "#cccccc" +","+ style[1] +","+ style[2] +","+ style[3]);
					}
					style = getStyle(currentCell, styles);
					style[2] = 3;	//change linewidth
					Plot.setStyle(currentCell, style[0] +","+ style[1] +","+ style[2] +","+ style[3]);
					Plot.freeze(false);
					selectWindow(RGB_overlay);
					hidden = true;
				}
				
			}
			else {
				roiManager("Select All");
				roiManager("Set Color", "gray");
				roiManager("Set Line Width", 1);
				run("Select None");
				selectWindow(kymograph_sorted);
				Overlay.remove;
				selectWindow(RGB_overlay);
			}
			if(hidden == true && flags&leftButton == 0) {
				unhide_traces(plot, styles);
				hidden = false;
			}
		}
		else if(getTitle() == kymograph_sorted || getTitle() == kymograph_sorted_smoothed) {
			setBatchMode(true);
			selectWindow(RGB_overlay);
			Stack.getPosition(channel, slice, frame);
			selectWindow(rank_vector);
			currentCell = getPixel(x, 0);	//rank_vector image starts at x=0
			if(currentCell != 0) showStatus("cell "+currentCell + 1);
			else showStatus("");
			
			show_line_on_sorted_kymograph(kymograph_sorted_smoothed, rank_array, currentCell, 2);
			show_line_on_sorted_kymograph(kymograph_sorted, rank_array, currentCell, 2);
//			show_horizontal_line_on_sorted_kymograph(kymograph_sorted, frame, 1);
//			show_horizontal_line_on_sorted_kymograph(kymograph_sorted_smoothed, frame, 1);
//			show_timeline_on_plot(plot, frame, 2);
			
			if(flags == 16) {	//leftclick to show a single trace
				selectWindow(plot);
				Plot.freeze(true);
				for (i = 0; i < nr_cells; i++) {
					//Plot.setStyle(i, styles[i]+",hidden");
					style = getStyle(i, styles);
					if(i != currentCell) Plot.setStyle(i, "#cccccc" +","+ style[1] +","+ style[2] +","+ style[3]);
				}
				style = getStyle(currentCell, styles);
				style[2] = 3;	//change linewidth
				Plot.setStyle(currentCell, style[0] +","+ style[1] +","+ style[2] +","+ style[3]);
				Plot.freeze(false);
				selectWindow(RGB_overlay);
				hidden = true;
			}

			style = getStyle(currentCell, styles);
			if(currentCell != oldCell && currentCell != 0) {
				style = getStyle(currentCell, styles);
				selectWindow(RGB_overlay);
				roiManager("select", currentCell);
				if(useTraceColor == true) roiManager("Set Color", style[0]);
				else roiManager("Set Color", "white");
				roiManager("Set Line Width", 3);
				if(oldCell != -1) {
					roiManager("select", oldCell);
					roiManager("Set Color", "gray");
					roiManager("Set Line Width", 1);
					run("Select None");
				}		
			}
			if(hidden == true && flags&leftButton == 0) {
				unhide_traces(plot, styles);
				hidden = false;
			}

			//left+ctrl to select multiple traces 
			selectWindow(kymograph_sorted);
			xa = x;
			xb = x;
			ctrl = false;
			while(flags == 18) {
				getCursorLoc(xb, y, z, flags);
				ctrl = true;
				Overlay.remove;
				if(xa != xb) {
					makeRectangle(minOf(xa, xb), 0, abs(xb-xa), height);
					Overlay.addSelection("", 0, "#55000000");
					run("Select None");
					wait(10);
				}
			}
			if(ctrl == true && xa != xb) {
				print(xa, xb);
				if(xa < xb) selectedTraces = Array.slice(rank_array, maxOf(0,xa), minOf(styles.length, xb));	//prevent crash if cursor is outside the image
				else selectedTraces = Array.slice(rank_array, maxOf(0,xb), minOf(styles.length, xa));
				Array.print(selectedTraces);
				selectWindow(plot);
				Plot.freeze(true);
				for (i = 0; i < nr_cells; i++) {
					Plot.setStyle(i, styles[i]+",hidden");
				}
				for (i = 0; i < selectedTraces.length; i++) {
					style = getStyle(selectedTraces[i], styles);
					style[2] = 3;	//change linewidth
					Plot.setStyle(selectedTraces[i], style[0] +","+ style[1] +","+ style[2] +","+ style[3]);
				}
				Plot.freeze(false);
				
				highlight_cells_in_RGB_overlay();
				selectWindow(RGB_overlay);
				getLocationAndSize(x, y, width, height);
				selection = "kymograph";
				create_selected_cells_table(selection);
				setLocation(x + width + 10, y);

				hidden = true;
				while(!isKeyDown("space")) {
					wait(10);
				}
				ctrl = false;
				selectWindow(RGB_overlay);
				roiManager("Select All");
				roiManager("Set Color", "gray");
				roiManager("Set Line Width", 1);
				run("Select None");
				selectWindow(kymograph_sorted);
			}	
		}
		oldCell = currentCell;
		selectWindow(RGB_overlay);
		Stack.getPosition(channel, slice, frame);
		selectWindow(kymograph_sorted);
		show_horizontal_line_on_sorted_kymograph(kymograph_sorted, frame, 1);
		show_horizontal_line_on_sorted_kymograph(kymograph_sorted_smoothed, frame, 1);
		setBatchMode(false);
		wait(25);
		getCursorLoc(x, y, z, flags);
	}
	run("Select None");
	setOption("DisablePopupMenu", false);
}
//END

function generate_rgn_file(name, hitList) {
	preamble = "<StageOverviewRegions>\n<Regions>\n<ShapeList>\n<Items>\n<Item0>\n<Name>hits</Name>\n<Identifier>"+generate_UUID()+"</Identifier>\n<Type>CompoundShape</Type>\n<Font />\n<Verticies>\n<Items />\n</Verticies>\n<DecoratorColors>\n<Items />\n</DecoratorColors>\n<ExtendedProperties>\n<Items />\n</ExtendedProperties>\n<Children>\n<Items>\n";
	postamble = "</Items>\n</Children>\n</Item0>\n</Items>\n<FillMaskMode>None</FillMaskMode>\n<VertexUnitMode>Pixels</VertexUnitMode>\n</ShapeList>\n</Regions>\n<StackList>\n";
	stacklist = "";
	output = File.directory;	//Location of the last opened file - NOT FAIL SAVE!
	if(File.exists(output + "HITS_"+name+".rgn")) File.delete(output + "HITS_"+name+".rgn");
	file = File.open(output + "HITS_"+name+".rgn");
	print(file, preamble);
	selectWindow(hitList);
	nrHits = Table.size;
	for (i = 0; i < nrHits; i++) {
		UUID = generate_UUID();
		start = "<Item"+i+">\n<Number>"+i+1+"</Number>\n<Tag>Cell_"+i+1+"</Tag>\n<Identifier>"+UUID+"</Identifier>\n<Type>Point</Type>\n<Fill>R:1,G:0,B:0,A:0</Fill>\n<Font />\n<Verticies>\n<Items>\n<Item0>\n";
		middle = "\<X\>" + d2s(Table.get("AbsPosX (m)", i),10) + "\</X\>\n\<Y\>"+ d2s(Table.get("AbsPosY (m)", i),10) + "\</Y\>\n";
		end = "</Item0>\n</Items>\n</Verticies>\n<DecoratorColors>\n<Items />\n</DecoratorColors>\n<ExtendedProperties>\n<Items />\n</ExtendedProperties>\n</Item"+i+">\n";
		print(file, start+middle+end);
		stacklist += "<Entry Identifier=\""+UUID+"\" Begin=\"0.0000000000\" End=\"0.0000000000\" SectionCount=\"0\" ReferenceX=\"0.0000000000\" ReferenceY=\"0.0000000000\" FocusStabilizerOffset=\"0.0000000000\" FocusStabilizerOffsetFixed=\"false\" StackValid=\"false\" Marked=\"false\" />\n";
	}
	print(file, postamble);
	print(file, stacklist);
	print(file, "</StackList>\n</StageOverviewRegions>\n");
	File.close(file);
	print(".RGN file saved as " + output + "HITS_"+name+".rgn");
}


function generate_UUID() {
	string = "";
	for (i = 0; i < 36; i++) {
		if(i==8 || i==13 || i==18 || i==23) string += "-";
		else string += toHex(random*16);
	}
	return string;
}


function highlight_cells_in_RGB_overlay() {
	selectWindow(RGB_overlay);
	roiManager("Set Color", "gray");
	roiManager("Set Line Width", 1);
	for (n = 0; n < selectedTraces.length; n++) {
		roiManager("select", selectedTraces[n]);
		if(useTraceColor == true) {
			style = getStyle(selectedTraces[n], styles);
			roiManager("Set Color", style[0]);
		}
		else roiManager("Set Color", "white");
		roiManager("Set Line Width", 3);
	}
	roiManager("Show All without labels");
	getLocationAndSize(x, y, width, height);
}


function create_selected_cells_table(selection) {
	//Create a table with selected cells
	cellCoordinatesTable = "Cell_coordinates.tsv";
	if(isOpen(cellCoordinatesTable)) use_coordinates = true;
	selectedCellsTable = "Selected_cells";
	Table.create("Selected_cells");
	if(selection == "plot" || selection == "kymograph") {
		Ext.CLIJ2_push(labelmap);
		Ext.CLIJ2_statisticsOfLabelledPixels(labelmap, labelmap);
		Ext.CLIJ2_release(labelmap);
		X = Table.getColumn("CENTROID_X", "Results");
		Y = Table.getColumn("CENTROID_Y", "Results");
		selectWindow("Results");
		run("Close");
	}
	for (n = 0; n < selectedTraces.length; n++) {
		if(use_coordinates == true) {
			selectedCell = Table.get("Cell total", selectedTraces[n], cellCoordinatesTable);
			absPosX = Table.get("AbsPosX (m)", selectedTraces[n], cellCoordinatesTable);
			absPosY = Table.get("AbsPosY (m)", selectedTraces[n], cellCoordinatesTable);
			Table.set("Cell", n, selectedCell+1, selectedCellsTable);
			Table.set("AbsPosX (m)", n, absPosX, selectedCellsTable);
			Table.set("AbsPosY (m)", n, absPosY, selectedCellsTable);
		}
		else {
			Table.set("Cell", n, selectedTraces[n]+1, selectedCellsTable);
			Table.set("PosX (px)", n, X[n], selectedCellsTable);
			Table.set("PosY (px)", n, Y[n], selectedCellsTable);
		}
	}
	Table.update;
	Table.setLocationAndSize(x + width + 10, y, 250, 400);
	if(use_coordinates == true) generate_rgn_file(basename, selectedCellsTable);
}


function show_line_on_sorted_kymograph(image, rank_array, currentCell, thickness) {
	currentWindow = getTitle();
	selectWindow(image);
	Overlay.remove;
	currentSortedCell = firstIndexOfArray(rank_array, currentCell);
	makeLine(currentSortedCell, 0, currentSortedCell, getHeight, thickness);
	Roi.setStrokeColor("black");
//	Roi.setStrokeWidth(2);
	run("Add Selection...");
//	Roi.setStrokeWidth(0);
	run("Select None");
	selectWindow(currentWindow);
}


function show_horizontal_line_on_sorted_kymograph(image, position, thickness) {
	currentWindow = getTitle();
	selectWindow(image);
	makeLine(0, position, getWidth(), position, thickness);
	Roi.setStrokeColor("black");
//	Roi.setStrokeWidth(2);
	run("Add Selection...");
//	Roi.setStrokeWidth(0);
	run("Select None");
	selectWindow(currentWindow);
}


function show_timeline_on_plot(plot, frame, thickness) {
	currentWindow = getTitle();
	selectWindow(plot);
	Plot.getLimits(xMin, xMax, yMin, yMax);
	Plot.drawLine(frame*frameInterval/xMax, yMin, frame*frameInterval/xMax, yMax);
	Plot.update();
	selectWindow(currentWindow);
}


function unhide_traces(plot, styles) {
	currentWindow = getTitle();
	selectWindow(plot);
	Plot.freeze(true);
	for (i = 0; i < styles.length; i++) {
		style = getStyle(i, styles);
		unhiddenStyle = style[0] +","+ style[1] +","+ style[2] +","+ style[3];
		Plot.setStyle(i, unhiddenStyle);
	}
	Plot.freeze(false);
	selectWindow(currentWindow);
}


//Construct plot of all traces from the kymograph image
function plot_traces_from_kymograph(template_plot, kymograph_all, selectedTraces) {
	selectWindow(kymograph_all);
	nr_cells = getWidth();
	y_axis = "Lifetime (ns)";

	getDimensions(width, height, channels, slices, frames);
	timeArray = Array.getSequence(height);
	timeArray = multiplyArraywithScalar(timeArray, frameInterval);
//	plotName = substring(saveName, 0, saveName.length-4) + " (lifetime traces plot)";
	plot_new = template_plot;
	selectWindow(template_plot);
	Plot.getLimits(xMin, xMax, yMin, yMax);
	call("ij.gui.ImageWindow.setNextLocation", 100, 50);
	Plot.create(plot_new, "", "");
	Plot.useTemplate(template_plot);
	Plot.setLimits(xMin, xMax, yMin, yMax);

	//setBatchMode(true);
	selectWindow(kymograph_all);

	run("Rotate 90 Degrees Left");
//	run("Flip Vertically", "stack");
	getDimensions(width, height, channels, slices, frames);
	n=0;
	for (s = 0; s < slices; s++) {
	    if(slices>1) Stack.setSlice(s+1);
		for (i = 0; i < height; i++) {
			makeRectangle(0, i, width, 1);
			lifetimeData = getProfile();
			if(lifetimeData[0] != 0) {
				color = getLabelColor(i, height);
				Plot.setColor(color);
				if(occursInArray(selectedTraces, n)) Plot.setLineWidth(selectedTraceLineWidth);
				else Plot.setLineWidth(1.0);
				Plot.add("line", timeArray, lifetimeData);
				n++;
			}
		}
	}
	Plot.show();
	setBatchMode("show");
	selectWindow(kymograph_all);
	run("Rotate 90 Degrees Right");
//	run("Flip Vertically", "stack");
	getDimensions(width, height, channels, slices, frames);
	run("Select None");
	setBatchMode("show");
	return plot_new;
}


//Construct plot of all traces from a Results table
function plot_traces(plotName, resultsTable, nr_cells, create_selected_plot, selectedTraces) {
	getLocationAndSize(xloc, yloc, width, height);
	//timepoints = Table.size(resultsTable);
	plotName2 = plotName+"_temp";
	selectWindow(plotName);
	Plot.getLimits(xMin, xMax, yMin, yMax);
	Plot.create(plotName2, "", "");
	Plot.useTemplate(plotName);
	Plot.setLimits(xMin, xMax, yMin, yMax);
//	Plot.setFrameSize(900, 600);
	if(create_selected_plot == false) {
		for (i = 0; i < nr_cells; i++) {
			lifetimeData = Table.getColumn("Y"+i, resultsTable);
			if(lifetimeData[0] != 0) {
				style = getStyle(i, styles);
				Plot.setColor(style[0]);
				Plot.add("line", timeArray, lifetimeData);
			}
		}
	}
	else {
		for (i = 0; i < nr_cells; i++) {
			if(occursInArray(selectedTraces, i)) {
				lifetimeData = Table.getColumn("Y"+i, resultsTable);
				if(lifetimeData[0] != 0) {
					style = getStyle(i, styles);
					Plot.setColor(style[0]);
					Plot.add("line", timeArray, lifetimeData);
				}
			}
		}		
	}
	if(create_selected_plot == false) close(plotName);
	Plot.show();
	selectWindow(plotName2);
	rename(plotName);
	setLocation(xloc, yloc);
	setBatchMode("show");
	return plotName;
}


function plot_single_cell_trace(plotName, resultsTable, cellToPlot) {
	selectWindow(plot);	//The plot with all traces
	getLocationAndSize(xloc, yloc, width, height);
	Plot.getLimits(xMin, xMax, yMin, yMax);
	call("ij.gui.ImageWindow.setNextLocation", xloc, yloc);
	Plot.create(plotName, "", "");
	Plot.useTemplate(plot);
	Plot.setLimits(xMin, xMax, yMin, yMax);

	lifetimeData = Table.getColumn("Y"+cellToPlot, resultsTable);
	Plot.setColor(style[0]);
	Plot.setLineWidth(selectedTraceLineWidth);
	Plot.add("line", timeArray, lifetimeData);
	Plot.show();
	setBatchMode("show");
	return plotName;
}


//Gets the 'content style' of the dataset 'label' from the styles array, and return it as an array
function getStyle(label, styles) {
	style_array = split(styles[label], ",");
	return style_array;
}

//Select an image with a name that contains the string argument
function select_image_containing_string(name) {
	imageList = getList("image.titles");
	for(i=0 ; i<imageList.length ; i++) {
		if(matches(imageList[i],".*"+name+".*")) {
			selectWindow(imageList[i]);
			return getTitle();
		}
	}
	return;
}

function getLabelColor(label, nrOfLabels) {
	color1 = IJ.pad(toHex(reds[label/nrOfLabels*255]),2);
	color2 = IJ.pad(toHex(greens[label/nrOfLabels*255]),2);
	color3 = IJ.pad(toHex(blues[label/nrOfLabels*255]),2);
	labelColor = "#"+color1+color2+color3;
	return labelColor;
}

function occursInArray(array, value) {
	for(i=0; i<array.length; i++) {
		if(array[i] == value) return true;
	}
	return false;
}

//Multiplies all elements of an array with a scalar
function multiplyArraywithScalar(array, scalar) {
	multiplied_array=newArray(lengthOf(array));
	for (a=0; a<lengthOf(array); a++) {
		multiplied_array[a]= (array[a]) * (scalar);
	}
	return multiplied_array;
}

//Returns the first index at which a value occurs in an array
function firstIndexOfArray(array, value) {
	for (a=0; a<lengthOf(array); a++) {
		if (array[a]==value) {
			break;
		}
	}
	return a;
}

//Returns the values in a 1D image (m,1,1) or (1,m,1) as an array
function image1DToArray(image) {
	getDimensions(width, height, channels, slices, frames);
	array = newArray(maxOf(width, height));
	if(height == 1) {
		for(x=0; x<width; x++) {
			array[x] = getPixel(x, 0);
		}
	}
	else if (width == 1) {
		for(y=0; y<height; y++) {
			array[y] = getPixel(0, y);
		}
	}
	else exit("Error in function 'image1DToArray': 1D image expected");
	return array;
}

}


macro "Select Traces Tool Options" {
	Dialog.create("Select Traces Options");
	Dialog.addRadioButtonGroup("", newArray("Single trace onMouseOver", "Select multiple traces"), 2, 1, "Single trace onMouseOver");
	Dialog.addMessage("\n");
	Dialog.addNumber("Smooth traces (mean filter radius)", smoothTraces);
	Dialog.addNumber("Approximate cell size (for point selections)", cellSelectionSize);
	Dialog.addNumber("Line thickness of selected traces", selectedTraceLineWidth);
	Dialog.addCheckbox("Use trace color to highlight cells in RGB overlay? (otherwise white)", useTraceColor);

	Dialog.show();
	operationMode = Dialog.getRadioButton();
	smoothTraces = Dialog.getNumber();
	selectionCellSize = Dialog.getNumber();
    selectedTraceLineWidth = Dialog.getNumber();
    useTraceColor = Dialog.getCheckbox();
}
