//@ File (label="Select a file", style="file") myFile
import groovy.xml.DOMBuilder
import loci.formats.ImageReader
import loci.formats.MetadataTools
import loci.formats.meta.IMetadata


class Get_czi_channels {
    String[] run(File myFile) {
        def reader = new ImageReader()
        IMetadata mymetadatastore = MetadataTools.createOMEXMLMetadata()
        reader.setMetadataStore(mymetadatastore)
        reader.setId(myFile.toString())
        def xmlstring = mymetadatastore.dumpXML()
        def xmlreader = new StringReader(xmlstring)
        def doc = DOMBuilder.parse(xmlreader)
        def element = doc.documentElement
        ArrayList<String> returnval = []
        def go = true
        element.getChildNodes().each {image->
            if (image.getNodeName()=='Image' && go){
                image.getChildNodes().each {pixels->
                    if (pixels.getNodeName()=='Pixels' && go){
                        pixels.each {channel->
                            if (channel.getNodeName()=='Channel'){
                                def attr = channel.getAttributes()
                                returnval.add(attr.getNamedItem("Name").getNodeValue())
                                go = false
                            }
                        }
                    }
                }
            }
        }
        return returnval.toList()
    }
}
