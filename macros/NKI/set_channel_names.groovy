import ij.CompositeImage
import java.awt.Color
import ij.process.LUT

class SetChannelNames {
    static void run(CompositeImage myimp, File csvfile){
        def mystack = myimp.getStack()
        def rows = csvfile.readLines()*.split(';')
        def header = rows[0]
        def name_idx = header.findIndexOf {it.compareToIgnoreCase('Name')}
        def color_idx = header.findIndexOf {it.compareToIgnoreCase('Color')}
        def thr_min_idx = header.findIndexOf {it.compareToIgnoreCase('Threshold Min')}
        def thr_max_idx = header.findIndexOf {it.compareToIgnoreCase('Threshold Max')}
        rows.tail().eachWithIndex{ String[] entry, int i ->
            def idx = myimp.getStackIndex(i+1, 1, 1)
            mystack.setSliceLabel(entry[name_idx], idx)
            def mylut = LUT.createLutFromColor(Color.decode(entry[color_idx]))
            mylut.min = Integer.parseInt(entry[thr_min_idx])
            mylut.max = Integer.parseInt(entry[thr_max_idx])
            myimp.setChannelLut(mylut, idx)
        }
        myimp.updateAndRepaintWindow()
    }
}
