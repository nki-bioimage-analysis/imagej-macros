import ij.IJ
import net.imglib2.Cursor
import net.imglib2.histogram.Histogram1d
import net.imglib2.histogram.Real1dBinMapper
import net.imglib2.histogram.BinMapper1d
import net.imglib2.histogram.Integer1dBinMapper
import net.imglib2.type.numeric.integer.AbstractIntegerType
import net.imglib2.type.numeric.integer.ShortType
import net.imglib2.type.numeric.real.AbstractRealType
import net.imglib2.img.Img

import static java.lang.Math.sqrt

class GeneralNKI {
    public void replacepixels(Img input, double val, double rep){
        def iterator = input.iterator()
        def type = input.firstElement().createVariable()
        def valType = input.firstElement().createVariable()
        def repType = input.firstElement().createVariable()
        valType.setReal(val)
        repType.setReal(rep)
        while (iterator.hasNext()) {
            type = iterator.next()
            if (type.compareTo(valType) == 0) type.set(repType)
        }
    }

    public long computeMinMax(input, min, max) {
        return computeMinMax(input, min, max, Double.NaN)
    }
    public long computeMinMax(input, min, max, Double ignoreValue) {
        // if the ignoreValue is NaN we don't ignore values
        boolean ignoreValueB = !ignoreValue.isNaN()
        long N = 0
        // create a cursor for the image (the order does not matter)
        def iterator = input.iterator()
        // initialize min and max with the first image value
        def type = iterator.next()
        def ignType = type.copy()
        ignType.setReal(ignoreValue)
        min.set(type)
        max.set(type)
        // loop over the rest of the data and determine min and max value
        while (iterator.hasNext()) {
            type = iterator.next()
            if(ignoreValueB && type.compareTo(ignType)==0) continue
            N += 1
            if (type.compareTo(min) < 0) min.set(type)
            if (type.compareTo(max) > 0) max.set(type)
        }
        return N
    }
    public Histogram1d createHistogram(Img myImg){
        BinMapper1d mapper
        def firstelement = myImg.firstElement()
        IJ.log(firstelement.class.toString())
        if(firstelement instanceof AbstractIntegerType){
            IJ.log("Found integer type")
            mapper = new Integer1dBinMapper(firstelement.getMinValue() as long, firstelement.getMaxValue()+1 as long, false)
        }else if (firstelement instanceof AbstractRealType){
            IJ.log("Found real type")
            def min = firstelement.createVariable()
            def max = firstelement.createVariable()
            def N = computeMinMax(myImg, min, max)
            long numbins = sqrt(N as double) as long
            mapper = new Real1dBinMapper(min.getRealDouble(), max.getRealDouble(), numbins, false)
        }else{
            IJ.error("Datatype not supported")
        }
        def histogram = new Histogram1d(mapper)
        histogram.addData(myImg)
        return histogram
    }

    public  double mean(double[] arr) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        double sum = 0;
        for (double value : arr) {
            sum += value;
        }
        return sum / arr.length;
    }

    public  double std(double[] arr, double mean) {
        if (arr == null || arr.length == 0) {
            return 0;
        }
        double sum = 0;
        for (double value : arr) {
            sum += Math.pow(value-mean,2)
        }
        return Math.sqrt(sum/ (arr.length-1))
    }
}