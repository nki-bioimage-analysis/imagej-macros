//Get splits from a trackmateXML

import fiji.plugin.trackmate.io.TmXmlReader
import fiji.plugin.trackmate.Spot
import ij.IJ

class TrackmateGetSplits {
     static String run(File xmlfile, height=2, depth=2) {
         def result = []
         def res = new TmXmlReader(xmlfile)
         def model = res.getModel()
         model.getTrackModel()
         def trackIDs = model.getTrackModel().trackIDs(true)
         def spots = model.getSpots()
         for (id in trackIDs) {
             ArrayList<Integer> splits = []
             def track_edges = model.getTrackModel().trackEdges(id)
             def ids = [:]
             for (edge in track_edges){
                 Spot myspot = edge.getSource() as Spot
                 if (ids.containsKey(myspot.ID())){
                     splits.add(myspot.ID())
                 }else{
                     ids[myspot.ID()] = 1
                 }
             }
             IJ.log('found '+splits.size()+' splits in track '+id)
             for (split in splits){
                 def spot = spots.search(split)
                 IJ.log('splitspot: '+spot.ID())
                 def edges = model.getTrackModel().edgesOf(spot)
                 ArrayList<Integer> branch1 = []
                 ArrayList<Integer> branch2 = []
                 ArrayList<Integer> root = []
                 def branchID=0
                 for (e in edges){
                     if (e.getTarget().ID()==spot.ID()){
                         Spot rootspot = e.getSource() as Spot  //one below the splitpoint
                         if (depth>0){root.add(rootspot.ID())}
                         for (int i=1 ; i<depth; i++){
                             def rootedges = model.getTrackModel().edgesOf(rootspot)
                             if (rootedges.size()==1){
                                 root.add(0)
                             }else{
                                 Spot spot1= rootedges[0].getSource() as Spot
                                 Spot spot2= rootedges[1].getSource() as Spot
                                 if (spot1.ID()==rootspot.ID()){
                                     rootspot=spot2
                                 }else{
                                     rootspot=spot1
                                 }
                                 root.add(rootspot.ID())
                             }
                         }
                     }else{
                         ArrayList<Integer> branch = []
                         Spot branchspot = e.getTarget() as Spot
                         if (height>0){branch.add(branchspot.ID())}
                         for (int i=1 ; i<height; i++){
                             def branchedges = model.getTrackModel().edgesOf(branchspot)
                             if (branchedges.size()==1){
                                 branch.add(0)
                             } else{
                                 Spot spot1 = branchedges[0].getTarget() as Spot
                                 Spot spot2 = branchedges[1].getTarget() as Spot
                                 if (spot1.ID()==branchspot.ID()){
                                     branchspot=spot2
                                 }else{
                                     branchspot=spot1
                                 }
                                 branch.add(branchspot.ID())
                             }
                         }
                         if (branchID==0){
                             branchID=1
                             branch1 = branch
                         }else{
                             branch2 = branch
                         }
                     }
                 }
                 def splitIDs = [id]
                 def spotID = [spot.ID()]
                 splitIDs += root.reverse()+spotID+branch1+root.reverse()+spotID+branch2
                 result += splitIDs
                 IJ.log(splitIDs.toString())
             }
         }
         return result
    }
}
