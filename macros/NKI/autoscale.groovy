package NKI

import ij.ImagePlus
import ij.IJ
import ij.process.ImageProcessor

class Autoscale {
    public double[] getscale(ImagePlus myImp, Boolean skip_lowest, Double range) {
        ImageProcessor ip = myImp.getChannelProcessor()
		Double thr_min_val=0.5-range/2
        Double thr_max_val=0.5+range/2
        return getscale(ip,skip_lowest,thr_min_val, thr_max_val)
    }
    public double[] getscale(ImagePlus myImp, Boolean skip_lowest, Double min_range, Double max_range) {
        ImageProcessor ip = myImp.getChannelProcessor()
		Double thr_min_val=min_range
		Double thr_max_val=max_range
        return getscale(ip,skip_lowest,thr_min_val, thr_max_val)
    }
    public double[] getscale(ImageProcessor ip,Boolean skip_lowest,Double thr_min_val, Double thr_max_val){
        def stats = ip.getStats()
        def hist = stats.getHistogram()
        def nPix = stats.pixelCount
        def hist_start = 0
        if (skip_lowest){
            nPix-=hist[0]
            hist_start = 1
        }
        def thr_min = nPix*thr_min_val
        def thr_max = nPix*thr_max_val
        double[] thr = [0, 0]
        boolean[] found = [false, false]
        long s = 0
        double bin = stats.histMin
        for (int j=hist_start; j<hist.size();j++){
            s+=hist[j]
            bin+=stats.binSize
            if (!found[0]&&s>thr_min){
                thr[0]=bin
                found[0]=true
            }
            if (!found[1]&&s>=thr_max){
                thr[1]=bin
                found[1]=true
            }
        }
        return thr
    }
}
