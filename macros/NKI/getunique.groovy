//@ ImagePlus myImp
package NKI
import ij.ImagePlus
import ij.IJ

class Getunique {
    int[] run(ImagePlus myImp) {
        def stats = myImp.getRawStatistics()
        def hist = stats.histogram16
        def values = []
        for (int ibin = 0; ibin < hist.size(); ibin++) {
            if (hist[ibin] > 0) {
                values.add(ibin)
            }
        }
        IJ.log("hallo!")
        return values.toArray() as int[]
    }
}
