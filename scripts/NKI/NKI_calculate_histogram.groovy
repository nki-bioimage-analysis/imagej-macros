//@ Img myImg

import ij.IJ
import net.imglib2.histogram.Histogram1d
import net.imglib2.img.Img
import net.imglib2.type.numeric.integer.AbstractIntegerType
import net.imglib2.type.numeric.real.AbstractRealType

Img myImg=myImg

// load generalNKI class
def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/general.groovy')
def myClass = gcl.parseClass(file)
def GeneralNKI = myClass.newInstance()

Histogram1d histogram = GeneralNKI.createHistogram(myImg)
IJ.log('N Bins = '+histogram.getBinCount().toString())
def lowerbound = myImg.firstElement().createVariable()
def upperbound = myImg.firstElement().createVariable()
histogram.getLowerBound(0, lowerbound)
histogram.getUpperBound(0, upperbound)
double binwidth
if (lowerbound instanceof AbstractIntegerType) {
    binwidth = 1.0
} else if (lowerbound instanceof AbstractRealType) {
    binwidth = upperbound.getRealDouble() -lowerbound.getRealDouble()
}
IJ.log('binwidth = '+binwidth)
IJ.log(histogram.toLongArray().toString())
