//@ ImagePlus myImp
//@ File (label = ".csv file containing channel names", style="File") channelsFile
import ij.CompositeImage
import ij.ImagePlus

ImagePlus myImp = myImp
File channelsFile = channelsFile

def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/set_channel_names.groovy')
def clazz1 = gcl.parseClass(file)
def myclass = clazz1.newInstance()
myclass.run(myImp as CompositeImage, channelsFile)
