//@ Img myImg
//@ Double value
//@ Double replace

import net.imglib2.img.Img


Img myImg=myImg
Double value=value
Double replace=replace

// load generalNKI class
def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/general.groovy')
def myClass = gcl.parseClass(file)
def GeneralNKI = myClass.newInstance()

GeneralNKI.replacepixels(myImg, value, replace)