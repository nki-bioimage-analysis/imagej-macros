#@ File[] (label="Input files (max projections)", style="File") file_list
#@ File[] (label="Input corresponding deconvolved files", style="File") file_list_decon
#@ File (label="Output folder", style = "directory") output_folder
#@ Boolean (label="Extract only nuclei tracks already present in frame 1 (currently always true)", value=true) only_first
#@ String (value="<html><br>Nuclei segmentation settings<hr></html>", visibility="MESSAGE") stardist_message
#@ Boolean (label="Subtract median background before StarDist?", value=false) subtract_background
#@ Boolean (label="Add all channels before StarDist", value=false) add_channels
#@ Integer (label="XY binning before StarDist", value=4) stardist_scaling
#@ Double (label="Nuclei detection probability (lower=more nuclei)", min=0, max=1, value=0.5) stardist_probability
#@ Integer (label="Foci diameter", value=5) foci_diameter
#@ Integer (label="Dilate labels with radius (pixels)", value=0) dilate_labels_radius

debugMode=false;

stardist_scaling = 1/stardist_scaling;	//invert scaling factor
print("\\Clear");
if(only_first == true) print("Retrieving nuclei tracks only present in the first frame.");

if(!File.exists(output_folder)) File.makeDirectory(output_folder);

for (i = 0; i < file_list.length; i++) {
	run("Close All");

	open(file_list[i]);
	image_org = getTitle();
	run("Enhance Contrast", "saturated=0.35");
	getDimensions(width, height, channels, slices, frames);

	//Subtract background as the median of every pixel over time (this works only in case cells move a lot) 
	if(subtract_background) run("NKI subtract median", "myImp="+image_org+", touint16=[false]");

	//Segment nuclei using StarDist. Add channels together (currently unnormalized) for better accuracy
	if(add_channels && channels>1) {
		run("Re-order Hyperstack ...", "channels=[Slices (z)] slices=[Channels (c)] frames=[Frames (t)]");
		run("Z Project...", "projection=[Average Intensity] all");
	}
	image_for_StarDist = getTitle();
	run("Enhance Contrast", "saturated=0.35");
	outliers_radius = 3*foci_diameter;
//	outliers_threshold = percentile_value(image_for_StarDist, 0.995);	//Determine the 0.5% highest pixels
	outliers_threshold = 5;
	
	run("NKI run stardist", "myImp="+image_for_StarDist+", channel=[1], scaling=["+stardist_scaling+"], min_label_size=[4000], max_label_size=[1000000], remove_outliers=[true], outliers_radius=["+outliers_radius+"], outliers_threshold=["+outliers_threshold+"], stardist_prob=["+stardist_probability+"], dilate_labels_radius=["+dilate_labels_radius+"]");
	rename("labelmap");
	labelmap_stardist = getTitle();
	run("glasbey_on_dark");
	setMinAndMax(0, 255);
	if(!debugMode) close("Label Image");
	if(!debugMode) close(image_for_StarDist);
	if(!debugMode) close(image_for_StarDist+"_scaled");
		
	run("NKI trackmate lblmap", "myImp="+labelmap_stardist+", allow_track_splitting=[false], gap_closing_max_distance=[200], linking_max_distance=[200], max_frame_gap=[3]");
	labelmap_tracked = getTitle();
	run("glasbey_on_dark");
	setMinAndMax(0, 255);
	
	if(!debugMode) close(labelmap_stardist);
	
	open(file_list_decon[i]);
	if(matches(file_list_decon[i], ".*cmle_ics.*")) run("Flip Vertically", "stack");	//Flip image back because Huygens messed it up
	image_decon = getTitle();

//	output_folder_file = output_folder + File.separator + substring(image_decon, lastIndexOf(image_decon, "_cmle.ics")-2, lastIndexOf(image_decon, "_cmle.ics"));
	output_folder_file = output_folder + File.separator + substring(image_decon, lastIndexOf(image_decon, ".ims")-2, lastIndexOf(image_decon, ".ims"));
	print("Saving extracted tracks into "+output_folder_file);
	if(!File.exists(output_folder_file)) File.makeDirectory(output_folder_file);

	selectWindow(labelmap_tracked);
	saveAs("tif", output_folder_file + File.separator + "labelmap_tracked.tif");
	labelmap_tracked = getTitle();
	File.makeDirectory(output_folder_file + File.separator + "tracks_original");
	tracks_folder = output_folder_file + File.separator + "tracks_original";

	run("NKI extract tracks", "intensityimp=["+image_decon+"], labelimp=["+labelmap_tracked+"], onlyfirst=[true], savefiles=[true], makecomposite=[false], to16bit=[false], output=["+tracks_folder+"]");

	register_tracks(tracks_folder);	//Maybe something else than outliers_threshold. It was set to 0.
	print("Finished processing "+image_org+" ("+i+1+"/"+file_list.length+")");
}


function register_tracks(tracks_folder) {
	registered_tracks_folder = File.getParent(tracks_folder) + File.separator + "tracks_registered";
	File.makeDirectory(registered_tracks_folder);
	combined_folder = File.getParent(tracks_folder) + File.separator + "tracks_combined";
	File.makeDirectory(combined_folder);
	tracklist = getFileList(tracks_folder);
	tracklist = Array.sort(tracklist);
	for (i = 0; i < tracklist.length; i++) {
		if(endsWith(tracklist[i], ".tif") && !File.isDirectory(tracks_folder + File.separator + tracklist[i])) {
			run("Close All");
			print("\\Update:Registering "+tracklist[i]);
			showProgress(tracklist[i], tracklist.length);	//assuming only .tif files present in the folder
			open(tracks_folder + File.separator + tracklist[i]);
			image = getTitle();

			if(bitDepth()!=32) run("32-bit");
			getDimensions(width, height, channels, slices, frames);

			colors = newArray("Red", "Green", "biop-Azure", "Grays");
			Stack.setFrame(frames/2);
			channelsString = "";
			for (c = 1; c <= channels; c++) {
				Stack.setChannel(c);
				run(colors[c-1]);
				run("NKI auto scale", "skip_lowest=[true] min_range=[0.2] max_range=[0.95]");
				channelsString += " channel"+c;
			}
			showStatus("Registering "+image+"...");

			Stack.setDisplayMode("composite");
			run("HyperStackReg ", "transformation=[Rigid Body]"+channelsString);	//Register using all channels
			Stack.setDisplayMode("color");
			
			for (c = 1; c <= channels; c++) {
				Stack.setChannel(c);
				for (t = 1; t <= frames; t++) {
					Stack.setFrame(t);
					changeValues(-0.01, 0.01, 0);	//Fix rounding errors of HyperStackReg
				}
				Stack.setFrame(frames/2);
			}
			rename(substring(image, 0, lastIndexOf(image, ".")) + "_registered");
			image_registered = getTitle();
			selectWindow(image_registered);
			for (c = 1; c <= channels; c++) {
				Stack.setChannel(c);
				run("NKI auto scale", "skip_lowest=[true] min_range=[0.8] max_range=[0.995]");
			}
			saveAs(registered_tracks_folder + File.separator + image_registered);

			run("Combine...", "stack1="+image+" stack2="+image_registered);
			
			for (c = 1; c <= channels; c++) {
				Stack.setChannel(c);
				run("NKI auto scale", "skip_lowest=[true] min_range=[0.8] max_range=[0.995]");
			}
			name = File.getNameWithoutExtension(combined_folder + File.separator + tracklist[i]) + "_combined";
			saveAs("tif", combined_folder + File.separator + name);
			close();
			close(image);
			close(image_registered);
		}
	}
}

function subtractMeanOfPixelsAboveZero(multiChannelImage, c, threshold) {
	selectWindow(multiChannelImage);
	run("Duplicate...", "title="+multiChannelImage+"_ch"+c+" duplicate channels="+c);
	image = multiChannelImage+"_ch"+c;
	Ext.CLIJ2_push(image);
	close(multiChannelImage+"_ch"+c);
	Ext.CLIJ2_meanOfPixelsAboveThreshold(image, threshold);
	meanAboveZero = getResult("Mean_of_pixels_above_threshold");
	meanAboveZero=0;	//Setting this back to zero, because it doesn't contribute much, and can lead to errors
	Ext.CLIJ2_addImageAndScalar(image, image_subtracted, -meanAboveZero);
	Ext.CLIJ2_replaceIntensity(image_subtracted, image_corrected, -meanAboveZero, 0);
	Ext.CLIJ2_pull(image_corrected);
	rename(image+"_corrected");
	Ext.CLIJ2_clear();
	return image+"_corrected";
}


//Find lower gray value of a (timelapse image at a certain percentile
function percentile_value(image, percentile) {
	selectWindow(image);
	getDimensions(width, height, channels, slices, frames);
	for (i = 0; i < frames; i++) {
		if(frames>1) Stack.setFrame(i+1);
		getRawStatistics(nPixels, mean, min, max, std, histogram);
		if(i > 0) histogram_all = addArrays(histogram_all, histogram);
		else histogram_all = Array.copy(histogram);
//		Array.print(histogram_all);
	}
	Stack.getStatistics(voxelCount, mean_stack, min_stack, max_stack, stdDev_stack);
	
	total = 0;
	bin=0;
	while (total < voxelCount*percentile) {
		total += histogram_all[bin];
		bin++;
	}
	return bin-1 - mean_stack;
}

//Adds two arrays of equal length element-wise
function addArrays(array1, array2) {
	added_array=newArray(maxOf(array1.length, array2.length));
	added_array = Array.copy(array1);
	for (a=0; a<minOf(array1.length, array2.length); a++) {
		added_array[a]=array1[a] + array2[a];
	}
	return added_array;
}
