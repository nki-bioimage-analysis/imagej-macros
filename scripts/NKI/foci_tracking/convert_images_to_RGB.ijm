#@ File[] (label = "Original image", style = "file") image_list
#@ File (label="Output folder", style = "directory") output_folder


for (f = 0; f < image_list.length; f++) {
	run("Close All");
	open(image_list[f]);
	run("RGB Color", "frames");
	//saveAs("tif", output_folder + File.separator + File.getNameWithoutExtension(image_list[f]) + "_overlay_RGB");
	run("AVI... ", "compression=JPEG frame=5 save=["+output_folder + File.separator + File.getNameWithoutExtension(image_list[f]) +".avi]");
}