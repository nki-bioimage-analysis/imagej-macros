run("CLIJ2 Macro Extensions", "cl_device=");
Ext.CLIJ2_clear();

if(bitDepth() != 32) run("32-bit");
image = getTitle();

//setBatchMode(true);
getDimensions(width, height, channels, slices, frames);
colors = newArray("Cyan", "Magenta", "Yellow", "Grays");
Stack.setFrame(frames/2);
channelsString = "";
for (c = 1; c <= channels; c++) {
	Stack.setChannel(c);
	run(colors[c-1]);
	run("NKI auto scale", "skip_lowest=true min_range=0.2 max_range=0.95");
	channelsString += "channel"+c;
}

showStatus("Registering "+image+"...");
run("HyperStackReg ", "transformation=[Rigid Body] "+channelsString);
getDimensions(width, height, channels, slices, frames);
for (c = 1; c <= channels; c++) {
	Stack.setChannel(c);
	for (t = 1; t <= frames; t++) {
		Stack.setFrame(t);
		changeValues(-0.01, 0.01, 0);	//Fix rounding errors of HyperStackReg
	}
	Stack.setFrame(frames/2);
	run("NKI auto scale", "skip_lowest=[true] min_range=[0.5] max_range=[0.98]");
}
//setBatchMode("show");
rename(substring(image, 0, lastIndexOf(image, ".")) + "_registered");
