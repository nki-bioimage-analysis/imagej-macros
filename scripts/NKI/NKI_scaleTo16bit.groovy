//@ ImagePlus myImp
//@ Float scale

import groovy.time.TimeCategory
import ij.IJ
import ij.ImagePlus
import ij.process.ImageConverter

ImagePlus myImp = myImp
Float scale = scale

def timeStart = new Date()
scaleTo16(myImp, scale)
def timeStop = new Date()
def duration = TimeCategory.minus(timeStop, timeStart)
IJ.log('ScaleTo16 took: ' + duration)

void scaleTo16(ImagePlus myImp, Float scale){
    def stats = myImp.getRawStatistics()
    if (stats.max*scale> 2**16){
        IJ.error('--scaleTo16-- Maximum of image would exceed 2**16 : '+stats.max*scale)
        return
    }
    def prc = myImp.getProcessor()
    prc * scale
    def ic = new ImageConverter(myImp)
    ic.setDoScaling(false)
    ic.convertToGray16()
    return
}

