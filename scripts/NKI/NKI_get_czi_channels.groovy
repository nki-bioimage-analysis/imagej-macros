//@ File (label="Select a file", style="file") myFile
import groovy.lang.GroovyClassLoader
import ij.IJ

File myFile=myFile
def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/get_czi_channels.groovy')
def clazz1 = gcl.parseClass(file)
def get_czi_channels = clazz1.newInstance()
IJ.log(get_czi_channels.run(myFile).toString())