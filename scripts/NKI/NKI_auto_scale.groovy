//@ ImagePlus myImp
//@ Boolean (label = "Ignore pixels in the lowest bin", value=true) skip_lowest
//@ Double (label = "Minimum percentile (between 0 and 1)", value=0.1) min_range
//@ Double (label = "Maximum percentile (between 0 and 1)", value=0.1) max_range


// This is a script to make the groovy class "Autoscale" macro-recordable.
import groovy.lang.GroovyClassLoader
import ij.ImagePlus

ImagePlus myImp=myImp
Boolean skip_lowest=skip_lowest
Double min_range=min_range
Double max_range=max_range

def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/autoscale.groovy')
def myClass = gcl.parseClass(file)
def autoscale = myClass.newInstance()
double[] thr = autoscale.getscale(myImp, skip_lowest, min_range, max_range)
myImp.setDisplayRange(thr[0], thr[1])
myImp.updateAndDraw()
