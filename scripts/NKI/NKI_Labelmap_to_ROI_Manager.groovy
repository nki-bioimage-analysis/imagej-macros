//@ ImagePlus (label = "labelmap") labelimp

import ij.IJ
import ij.ImagePlus
import net.haesleinhuepf.clij2.CLIJ2
import net.haesleinhuepf.clij2.plugins.StatisticsOfLabelledPixels
import ij.plugin.frame.RoiManager

CLIJ2 clij2 = CLIJ2.getInstance()
labelmap_cl = clij2.push(labelimp)

// Get largest bounding box for labels
int maxW = 0
int maxH = 0
def stats = clij2.statisticsOfLabelledPixels(labelmap_cl, labelmap_cl)
for (int j=0; j<stats.length ; j++){
    if (stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_WIDTH.value]>maxW){maxW=(int)stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_WIDTH.value]}
    if (stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_HEIGHT.value]>maxH){maxH=(int)stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_HEIGHT.value]}
}
def roi_label_cl = clij2.create([maxW, maxH])
def roi_mask_cl = clij2.create([maxW, maxH])

// reset RoiManager
def rm = new RoiManager()
rm = rm.getRoiManager()
rm.reset()
rm.setVisible(false)

for (int j=0; j<stats.length ; j++){
    if (stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_X.value]==Double.MAX_VALUE){continue}
    def i = (int) stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.MINIMUM_INTENSITY.value]
    def x = stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_X.value]
    def y = stats[j][StatisticsOfLabelledPixels.STATISTICS_ENTRY.BOUNDING_BOX_Y.value]
    clij2.crop2D(labelmap_cl, roi_label_cl, x.round(), y.round())
    clij2.labelToMask(roi_label_cl, roi_mask_cl, i)
    def myroi = clij2.pullAsROI(roi_mask_cl)
    myroi.setLocation(x,y)
    myroi.setName(i.toString())
    rm.addRoi(myroi)
}
rm.setVisible(true)
