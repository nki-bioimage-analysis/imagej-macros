//@ File xmlfile
//@ Integer frames_forward
//@ Integer frames_backward
import groovy.lang.GroovyClassLoader
import ij.IJ

File xmlfile=xmlfile
Integer frames_forward=frames_forward
Integer frames_backward=frames_backward

def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/trackmate_getsplits.groovy')
def clazz1 = gcl.parseClass(file)
def myclass = clazz1.newInstance()
IJ.log(myclass.run(xmlfile, frames_forward, frames_backward).toString())
