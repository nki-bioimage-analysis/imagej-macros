run("CLIJ2 Macro Extensions", "cl_device=");
Ext.CLIJ2_clear();

if(bitDepth() != 32) run("32-bit");
image = getTitle();

setBatchMode(true);
getDimensions(width, height, channels, slices, frames);
/*
//remove empty frames - wanted, but not for missing frames (still to fix)
// takes forever for a multi-channel image... in BatchMode even worse
for (f = frames; f >= 1; f--) {
	Stack.setFrame(f);
	if(getValue("Mean") == 0) {
		run("Delete Slice", "delete=frame");
		//f++;
		print(f);
	}
}
*/
setBatchMode(true);
mergeString = "";
for (c = 1; c <= channels; c++) {
	correctedImage = subtractMeanOfPixelsAboveZero(image, c);
	mergeString += "c"+c+"="+correctedImage+" ";
}
run("Merge Channels...", mergeString+" create");
run("Re-order Hyperstack ...", "channels=[Channels (c)] slices=[Frames (t)] frames=[Slices (z)]");
Stack.setChannel(2);
run("Green");
resetMinAndMax;
getMinAndMax(min, max);
setMinAndMax(0, max);
Stack.setChannel(1);
run("Magenta");
resetMinAndMax;
getMinAndMax(min, max);
setMinAndMax(0, max);
Stack.setDisplayMode("composite");
setBatchMode("show");

run("HyperStackReg ", "transformation=[Rigid Body] channel1 channel2");
setBatchMode("show");
rename(substring(image, 0, lastIndexOf(image, ".")) + "_registered");

function subtractMeanOfPixelsAboveZero(multiChannelImage, c) {
	selectWindow(multiChannelImage);
	run("Duplicate...", "title="+multiChannelImage+"_ch"+c+" duplicate channels="+c);
	image = multiChannelImage+"_ch"+c;
	Ext.CLIJ2_push(image);
	close(multiChannelImage+"_ch"+c);
	Ext.CLIJ2_meanOfPixelsAboveThreshold(image, 0);
	meanAboveZero = getResult("Mean_of_pixels_above_threshold");
	Ext.CLIJ2_addImageAndScalar(image, image_subtracted, -meanAboveZero);
	Ext.CLIJ2_replaceIntensity(image_subtracted, image_corrected, -meanAboveZero, 0);
	Ext.CLIJ2_pull(image_corrected);
	rename(image+"_corrected");
	Ext.CLIJ2_clear();
	return image+"_corrected";
}
