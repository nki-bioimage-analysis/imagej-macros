image = getTitle();
run("Cellpose Advanced");
rename(image+"_segmentation");
run("glasbey_on_dark");
run("NKI Labelmap to ROI Manager");
roiManager("Set Color", "#7700ffff");
selectImage(image);
run("Add Image...", "image=["+image+"_segmentation] x=0 y=0 opacity=33 zero");
roiManager("Show All without labels");