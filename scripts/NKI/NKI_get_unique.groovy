//@ ImagePlus myImp
import groovy.lang.GroovyClassLoader
import ij.IJ
import ij.ImagePlus

ImagePlus myImp=myImp
def gcl = new GroovyClassLoader()
File file = new File(ij.Menus.getMacrosPath()+'NKI/getunique.groovy')
def clazz1 = gcl.parseClass(file) 
def getunique = clazz1.newInstance()
IJ.log(getunique.run(myImp).toString())